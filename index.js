(function () {
    const sortDrinkByPrice = (drinks) =>
        drinks.sort((a, b) => a.price - b.price);

    drinks = [
        { name: 'lemonade', price: 50 },
        { name: 'lime', price: 10 },
    ];

    sortDrinkByPrice(drinks); //?
})();
